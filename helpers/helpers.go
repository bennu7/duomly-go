package helpers

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"gitlab.com/bennu7/duomly-go/interfaces"
	"golang.org/x/crypto/bcrypt"
	"regexp"
)

func HandleError(err error) {
	if err != nil {
		panic(err.Error())
	}
}

func HashAndSalt(pwd []byte) string {
	hash, err := bcrypt.GenerateFromPassword(pwd, bcrypt.MinCost)
	HandleError(err)
	return string(hash)
}

func ConnectDB() *gorm.DB {
	db, err := gorm.Open("postgres", "host=localhost port=5432 user=postgres dbname=duomly_go_bankapp password=belajar sslmode=disable")
	HandleError(err)

	return db
}

func Validation(values []interfaces.Validation) bool {
	username := regexp.MustCompile(`^([A-Za-z0-9]{5,})+$`)
	email := regexp.MustCompile(`^([a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+)$`)

	for _, value := range values {
		switch value.Valid {
		case "username":
			if !username.MatchString(value.Value) {
				return false
			}
		case "email":
			if !email.MatchString(value.Value) {
				return false
			}
		case "password":
			if len(value.Value) < 5 {
				return false
			}
		}
	}
	return true
}
