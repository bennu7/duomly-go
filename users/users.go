package users

import (
	"github.com/golang-jwt/jwt/v4"
	"gitlab.com/bennu7/duomly-go/helpers"
	"gitlab.com/bennu7/duomly-go/interfaces"
	"golang.org/x/crypto/bcrypt"
	"net/http"
	"time"
)

func prepareToken(user *interfaces.User) string {
	//Sign TOKEN JWT
	tokenContent := jwt.MapClaims{
		"user_id": user.ID,
		"expired": time.Now().Add(time.Minute * 30).Unix(),
	}
	jwtToken := jwt.NewWithClaims(jwt.SigningMethodHS256, tokenContent)
	token, err := jwtToken.SignedString([]byte("TokenPassword"))
	helpers.HandleError(err)

	return token
}

func prepareresponse(user *interfaces.User, accounts []interfaces.ResponseAccount) map[string]any {

	//Setup Response
	responseUser := &interfaces.ResponseUser{
		Id:       user.ID,
		Username: user.Username,
		Email:    user.Email,
		Accounts: accounts,
	}

	//prepare response
	var token = prepareToken(user)
	var response map[string]any
	response = map[string]any{
		"message": "All process ok!",
	}
	response["jwt"] = token
	response["data"] = responseUser

	return response
}

func Login(username string, pass string) map[string]any {
	// *validasi data menggunakan helper Validation custom
	valid := helpers.Validation(
		[]interfaces.Validation{
			{
				Value: username,
				Valid: "username",
			},
			{
				Value: pass,
				Valid: "password",
			},
		},
	)

	if valid {
		//Connect DB
		db := helpers.ConnectDB()
		defer db.Close()
		user := &interfaces.User{}
		if db.Where("username = ?", username).First(&user).RecordNotFound() {
			return map[string]any{
				"message": "User not found",
			}
		}

		//verify password
		passErr := bcrypt.CompareHashAndPassword([]byte(pass), []byte(user.Password))
		if passErr == bcrypt.ErrMismatchedHashAndPassword && passErr != nil {
			return map[string]any{
				"message": "Invalid login credentials. Please try again",
			}
		}

		//Find account for the user
		var accounts []interfaces.ResponseAccount
		db.Table("accounts").Select("id, name, balance").Where("user_id = ?", user.ID).Scan(&accounts)

		var response = prepareresponse(user, accounts)

		return response
	} else {
		return map[string]any{
			"message": "not valid values",
		}
	}

}

func Register(w http.ResponseWriter, username string, email string, pass string) map[string]any {
	valid := helpers.Validation(
		[]interfaces.Validation{
			{
				Value: username,
				Valid: "username",
			},
			{
				Value: pass,
				Valid: "password",
			},
			{
				Value: email,
				Valid: "email",
			},
		},
	)

	if valid {
		//Connect DB
		db := helpers.ConnectDB()
		defer db.Close()
		user := &interfaces.User{}

		//Check if user already exist
		if !db.Where("username = ?", username).First(&user).RecordNotFound() {
			w.WriteHeader(http.StatusConflict)

			return map[string]any{
				"message": "User already exist",
			}
		}

		//Check if email already exist
		if !db.Where("email = ?", email).First(&user).RecordNotFound() {
			w.WriteHeader(http.StatusConflict)

			return map[string]any{
				"message": "Email already exist",
			}
		}

		//Hash password
		hashPassword := helpers.HashAndSalt([]byte(pass))

		//Create user
		user = &interfaces.User{
			Username: username,
			Email:    email,
			Password: hashPassword,
		}

		db.Create(&user)

		return map[string]any{
			"message": "User created!",
			"data":    user,
		}

	} else {
		w.WriteHeader(http.StatusBadRequest)

		return map[string]any{
			"message": "not valid values",
		}
	}
}
