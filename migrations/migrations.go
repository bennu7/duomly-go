package migrations

import (
	"gitlab.com/bennu7/duomly-go/helpers"
	"gitlab.com/bennu7/duomly-go/interfaces"
)

func createAccounts() {
	db := helpers.ConnectDB()

	users := &[2]interfaces.User{
		{
			Username: "bennu",
			Email:    "bennu@gmail.com",
		},
		{
			Username: "l ibnu",
			Email:    "libnu@gmail.com",
		},
	}

	for i := 0; i < len(users); i++ {
		generatedPassword := helpers.HashAndSalt([]byte(users[i].Username))
		user := &interfaces.User{
			Username: users[i].Username,
			Email:    users[i].Email,
			Password: generatedPassword,
		}

		db.Create(&user)

		account := &interfaces.Account{
			Type:    "Daily Account",
			Name:    string(users[i].Username + "'s account"),
			Balance: uint(10000 * int(i+1)),
			UserID:  user.ID,
		}

		db.Create(&account)
	}

	defer db.Close()
}

func Migrate() {
	// * migrate user and account from
	User := &interfaces.User{}
	Account := &interfaces.Account{}

	db := helpers.ConnectDB()
	db.AutoMigrate(&User, &Account)

	defer db.Close()

	createAccounts()
}
