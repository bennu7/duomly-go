module gitlab.com/bennu7/duomly-go

go 1.19

require (
	github.com/jinzhu/gorm v1.9.16
	golang.org/x/crypto v0.6.0
)

require (
	github.com/golang-jwt/jwt/v4 v4.5.0 // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/lib/pq v1.1.1 // indirect
)
