package api

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"gitlab.com/bennu7/duomly-go/helpers"
	"gitlab.com/bennu7/duomly-go/users"
	"io"
	"log"
	"net/http"
)

type LoginRequest struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type RegisterRequest struct {
	Username string `json:"username"`
	Email    string `json:"email"`
	Password string `json:"password"`
}

type ErrorResponse struct {
	Message string `json:"message"`
}

func login(w http.ResponseWriter, r *http.Request) {
	//first, read body
	body, err := io.ReadAll(r.Body)
	helpers.HandleError(err)

	//Handle Login
	var formattedBody LoginRequest
	err = json.Unmarshal(body, &formattedBody)
	helpers.HandleError(err)
	login := users.Login(formattedBody.Username, formattedBody.Password)

	//Prepare reponse
	if login["message"] == "All process ok!" {
		resp := login
		json.NewEncoder(w).Encode(resp)
	} else {
		//Hanlde error
		resp := ErrorResponse{
			Message: "Wrong username or password, " + login["message"].(string) + "!",
		}
		json.NewEncoder(w).Encode(resp)
	}
}

func register(w http.ResponseWriter, r *http.Request) {
	//first, read body
	body, err := io.ReadAll(r.Body)
	helpers.HandleError(err)

	//Handle Register
	var formattedBody RegisterRequest
	err = json.Unmarshal(body, &formattedBody)
	helpers.HandleError(err)
	register := users.Register(w, formattedBody.Username, formattedBody.Email, formattedBody.Password)

	//Prepare reponse
	if register["message"] == "User created!" {
		w.WriteHeader(http.StatusCreated)

		resp := register
		json.NewEncoder(w).Encode(resp)
	} else {
		//Hanlde error
		resp := ErrorResponse{
			Message: "Error, " + register["message"].(string) + "!",
		}
		json.NewEncoder(w).Encode(resp)
	}
}

func StartApi() {
	router := mux.NewRouter()

	router.HandleFunc("/login", login).Methods(http.MethodPost)
	router.HandleFunc("/register", register).Methods(http.MethodPost)

	http := http.Server{
		Addr:    ":8080",
		Handler: router,
	}
	fmt.Println("Server started on port 8080")

	log.Fatal(http.ListenAndServe())
}
